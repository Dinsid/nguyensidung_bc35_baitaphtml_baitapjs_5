// bài 1: Quản lí tuyển sinh

function tinhTongDiem(a,b,c,d,e){
    var tongDiem = a + b+ c+ d +e;
    return tongDiem;
}

function ketQua() {
    var diemChuan = +document.getElementById('txt-diem-chuan').value;
    var diemMonThuNhat = +document.getElementById('txt-mon-1').value;
    var diemMonThuHai = +document.getElementById('txt-mon-2').value;
    var diemMonThuBa = +document.getElementById('txt-mon-3').value;
    var khuVuc = +document.getElementById('area').value;
    var doiTuong = +document.getElementById('object').value;
    var tongDiem = tinhTongDiem(diemMonThuNhat, diemMonThuHai, diemMonThuBa, khuVuc, doiTuong);
    var result_1 = document.getElementById('result-1');


    if(tongDiem>diemChuan){
        result_1.innerHTML = `Kết quả: Tổng điểm của thí sinh ${tongDiem}. Chúc mừng bạn đã đậu`;
    } else if(diemMonThuNhat == 0 || diemMonThuHai == 0 || diemMonThuBa == 0) {
        result_1.innerHTML = `Kết quả:  Rất tiếc bạn đã rớt`;
    } else {
        result_1.innerHTML = `Kết quả: Tổng điểm của thí sinh ${tongDiem}. Rất tiếc bạn đã rớt`;
    }
    
}

// Bài 2: Tính tiền điện


function tinhTien() {
    var hoTen = document.getElementById('txt-ho-ten').value;
    var soKw = +document.getElementById('txt-so-kw').value;
    var tienDien = 0;
    var result_2 = document.getElementById('result-2');

    if(soKw <= 50) {
        tienDien += soKw*500;
        result_2.innerHTML = `Họ tên: ${hoTen}; Tiền Điện: ${tienDien} đ`;
    } else if(soKw<=100) {
        tienDien += 50*500 + (soKw-50)*650;
        result_2.innerHTML = `Họ tên: ${hoTen}; Tiền Điện: ${tienDien} đ`;
    } else if(soKw<=200) {
        tienDien += 50*500 + 50*650 + (soKw-100)*850;
        result_2.innerHTML = `Họ tên: ${hoTen}; Tiền Điện: ${tienDien} đ`;
    } else if(soKw<=350) {
        tienDien += 50*500 + 50*650 + 100*850 + (soKw-150)*1100;
        result_2.innerHTML = `Họ tên: ${hoTen}; Tiền Điện: ${tienDien} đ`;
    } else {
        tienDien += 50*500 + 50*650 + 100*850 + 150*1100 + (soKw-350)*1300;
        result_2.innerHTML = `Họ tên: ${hoTen}; Tiền Điện: ${tienDien} đ`;
    }


}